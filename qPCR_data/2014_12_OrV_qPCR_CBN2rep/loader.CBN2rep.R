################################################################################
###qPCR prep script ############################################################
################################################################################

################################################################################
###Depends on: qPCR functions

    target.file <- read.delim("Target_qPCR_CBN2rep.txt", header = TRUE, stringsAsFactors = FALSE)
    data.file <- TargetLoading(target.file, "./Raw/")
        primer.names <- c("Orsay","Orsay","Orsay","Orsay","Y37E3.7","Y37E3.7","RPL-6","RPL-6")
        ref.primers <- c("Y37E3.7","RPL-6")

    ###The pre-processing function calculates mean, sd and n per primer over the technical replicates
    ###It also transforms the data to log2(2^(x-Ct))

    data.CBN2rep <- PreProcess(targets=target.file, data.input=data.file, primer.names=primer.names)

    ###Destroy the evidence!
    remove(target.file)
    remove(data.file)
    remove(primer.names)
    remove(ref.primers)
