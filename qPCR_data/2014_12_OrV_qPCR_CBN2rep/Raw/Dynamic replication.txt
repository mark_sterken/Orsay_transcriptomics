number	filename	column_on_plate	Strain	Strain_type	Experiment_date	Experiment_batch	Experiment_type	Virus_stock	Virus_stock_amount_uL	Virus_incubation_time	Generation	Age_infection	Stage_infection	Age_isolation	Stage_isolation	Date_RNA_isolation	ng/ul 	A260 	A280 	260/280 	260/230 	cDNA_amount	Experiment	Comments	Data.status	Paper					
580	Wannisa_qPCR008_2014-12-4	1	N2	WT	20141202	2	Mock	20141003	0	1	NA	26	L2	56	Young_adult	20141204	965.57	24.139	10.719	2.25	2.47	500	Replication		GoodToGo						
581	Wannisa_qPCR008_2014-12-4	2	N2	WT	20141202	2	Virus	20141003	100	1	NA	26	L2	29	L2	20141204	313.7	7.842	3.56	2.2	2.34	500	Replication		GoodToGo						
582	Wannisa_qPCR008_2014-12-4	3	N2	WT	20141202	2	Virus	20141003	100	1	NA	26	L2	32	L2	20141204	610.89	15.272	6.829	2.24	2.37	500	Replication		GoodToGo						
583	Wannisa_qPCR008_2014-12-4	4	N2	WT	20141202	2	Virus	20141003	100	1	NA	26	L2	46	L4	20141204	1127.78	28.194	12.594	2.24	2.44	500	Replication		GoodToGo						
584	Wannisa_qPCR008_2014-12-4	5	N2	WT	20141202	2	Virus	20141003	100	1	NA	26	L2	56	Young adult	20141204	736.41	18.41	8.185	2.25	2.4	500	Replication		GoodToGo						
585	Wannisa_qPCR008_2014-12-4	6	CB4856	WT	20141202	2	Mock	20141003	0	1	NA	26	L2	56	Young adult	20141204	787.33	19.683	8.895	2.21	2.41	500	Replication		GoodToGo						
586	Wannisa_qPCR008_2014-12-4	7	CB4856	WT	20141202	2	Virus	20141003	100	1	NA	26	L2	29	L2	20141204	389.93	9.748	4.487	2.17	2.23	500	Replication		GoodToGo						
587	Wannisa_qPCR008_2014-12-4	8	CB4856	WT	20141202	2	Virus	20141003	100	1	NA	26	L2	32	L2	20141204	168.66	4.217	1.904	2.21	2.31	500	Replication		GoodToGo						
588	Wannisa_qPCR008_2014-12-4	9	CB4856	WT	20141202	2	Virus	20141003	100	1	NA	26	L2	46	L4	20141204	547.64	13.691	6.154	2.22	2.33	500	Replication		GoodToGo						
589	Wannisa_qPCR008_2014-12-4	10	CB4856	WT	20141202	2	Virus	20141003	100	1	NA	26	L2	56	Young adult	20141204	525.52	13.138	5.876	2.24	2.31	500	Replication		GoodToGo						
613	Wannisa_qPCR011_2014-12-11	1	N2	WT	20141209	3	Mock	20141003	0	1	NA	26	L2	61	Adult	20141211	541.4	13.535	6.038	2.24	2.42	500	Replication		GoodToGo						
614	Wannisa_qPCR011_2014-12-11	2	N2	WT	20141209	3	Virus	20141003	100	1	NA	26	L2	31	L2	20141211	291.22	7.28	3.331	2.19	2.38	500	Replication		GoodToGo						
615	Wannisa_qPCR011_2014-12-11	3	N2	WT	20141209	3	Virus	20141003	100	1	NA	26	L2	34	L3	20141211	352.07	8.802	4.027	2.19	2.36	500	Replication		GoodToGo						
616	Wannisa_qPCR011_2014-12-11	4	N2	WT	20141209	3	Virus	20141003	100	1	NA	26	L2	53	L4	20141211	500.78	12.52	5.581	2.24	2.42	500	Replication		GoodToGo						
617	Wannisa_qPCR011_2014-12-11	5	N2	WT	20141209	3	Virus	20141003	100	1	NA	26	L2	61	Adult	20141211	797.14	19.929	8.906	2.24	2.45	500	Replication		GoodToGo						
618	Wannisa_qPCR011_2014-12-11	6	CB4856	WT	20141209	3	Mock	20141003	0	1	NA	26	L2	61	Adult	20141211	589.95	14.749	6.562	2.25	2.4	500	Replication		GoodToGo						
619	Wannisa_qPCR011_2014-12-11	7	CB4856	WT	20141209	3	Virus	20141003	100	1	NA	26	L2	31	Adult	20141211	211.87	5.297	2.422	2.19	2.37	500	Replication		GoodToGo						
620	Wannisa_qPCR011_2014-12-11	8	CB4856	WT	20141209	3	Virus	20141003	100	1	NA	26	L2	34	L3	20141211	132.9	3.322	1.53	2.17	2.34	500	Replication		GoodToGo						
621	Wannisa_qPCR011_2014-12-11	9	CB4856	WT	20141209	3	Virus	20141003	100	1	NA	26	L2	53	L4	20141211	181.65	4.541	2.054	2.21	2.41	500	Replication		GoodToGo						
622	Wannisa_qPCR011_2014-12-11	10	CB4856	WT	20141209	3	Virus	20141003	100	1	NA	26	L2	61	Adult	20141211	878.65	21.966	9.848	2.23	2.43	500	Replication		GoodToGo						
623	Wannisa_qPCR012_2014-12-12	1	N2	WT	20141127	1	Virus	20141003	100	1	NA	26	L2	41	L4	20141201	772.21	19.305	8.732	2.21	2.39	500	Replication	*no mock infection in both N2 and CB4856	GoodToGo						
624	Wannisa_qPCR012_2014-12-12	2	N2	WT	20141127	1	Virus	20141003	100	1	NA	26	L2	44	L4	20141201	774.01	19.35	8.702	2.22	2.41	500	Replication	** ran qPCR after batch 2 and 3 because of the impaired machine	GoodToGo						
625	Wannisa_qPCR012_2014-12-12	3	N2	WT	20141127	1	Virus	20141003	100	1	NA	26	L2	46	L4	20141201	188.65	4.716	2.142	2.2	2.36	500	Replication		GoodToGo						
626	Wannisa_qPCR012_2014-12-12	4	N2	WT	20141127	1	Virus	20141003	100	1	NA	26	L2	48	L4	20141201	693.93	17.348	7.77	2.23	2.43	500	Replication		GoodToGo						
627	Wannisa_qPCR012_2014-12-12	5	CB4856	WT	20141127	1	Virus	20141003	100	1	NA	26	L2	41	L4	20141201	640.01	16	7.207	2.22	2.38	500	Replication		GoodToGo						
628	Wannisa_qPCR012_2014-12-12	6	CB4856	WT	20141127	1	Virus	20141003	100	1	NA	26	L2	44	L4	20141201	702.92	17.573	7.923	2.22	2.32	500	Replication		GoodToGo						
629	Wannisa_qPCR012_2014-12-12	7	CB4856	WT	20141127	1	Virus	20141003	100	1	NA	26	L2	46	L4	20141201	794	19.85	8.967	2.21	2.34	500	Replication		GoodToGo						
630	Wannisa_qPCR012_2014-12-12	8	CB4856	WT	20141127	1	Virus	20141003	100	1	NA	26	L2	48	L4	20141201	763.3	19.083	8.52	2.24	2.37	500	Replication		GoodToGo						
643	Wannisa_qPCR014_2014-12-18	1	N2	WT	20141216	4	Mock	20141003	0	1	NA	26	L2	59	Young adult	20141218	392.01	9.8	4.492	2.18	2.43	500	Replication	put the plate usside down in qPCR	GoodToGo						
644	Wannisa_qPCR014_2014-12-18	2	N2	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	48	L4	20141218	321.04	8.026	3.671	2.19	2.38	500	Replication		GoodToGo						
645	Wannisa_qPCR014_2014-12-18	3	N2	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	51	L4	20141218	199.96	4.999	2.283	2.19	2.34	500	Replication		GoodToGo						
646	Wannisa_qPCR014_2014-12-18	4	N2	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	53	L4	20141218	438.41	10.96	5.029	2.18	2.38	500	Replication		GoodToGo						
647	Wannisa_qPCR014_2014-12-18	5	N2	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	55	L4	20141218	439.05	10.976	5.016	2.19	2.4	500	Replication		GoodToGo						
648	Wannisa_qPCR014_2014-12-18	6	N2	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	59	Young adult	20141218	578.19	14.455	6.496	2.23	2.46	500	Replication		GoodToGo						
649	Wannisa_qPCR014_2014-12-18	7	CB4856	WT	20141216	4	Mock	20141003	0	1	NA	26	L2	59	Young adult	20141218	263.55	6.589	2.983	2.21	2.44	500	Replication		GoodToGo						
650	Wannisa_qPCR014_2014-12-18	8	CB4856	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	48	L4	20141218	261.43	6.536	2.981	2.19	2.37	500	Replication		GoodToGo						
651	Wannisa_qPCR014_2014-12-18	9	CB4856	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	51	L4	20141218	175.02	4.375	1.998	2.19	2.27	500	Replication		GoodToGo						
652	Wannisa_qPCR014_2014-12-18	10	CB4856	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	53	L4	20141218	223.18	5.579	2.607	2.14	2.03	500	Replication		GoodToGo						
653	Wannisa_qPCR014_2014-12-18	11	CB4856	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	55	L4	20141218	154.59	3.865	1.767	2.19	2.22	500	Replication		GoodToGo						
654	Wannisa_qPCR014_2014-12-18	12	CB4856	WT	20141216	4	Virus	20141003	100	1	NA	26	L2	59	Young adult	20141218	299.52	7.488	3.419	2.19	2.34	500	Replication		GoodToGo						
667	Wannisa_qPCR016_2014-12-29	1	N2	WT	20141223	5	Mock	20141003	0	1	NA	26	L2	54	L4	20141229	288.24	7.206	3.256	2.21	2.41	500	Replication		GoodToGo						
668	Wannisa_qPCR016_2014-12-29	2	N2	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	54	L4	20141229	247.22	6.181	2.814	2.2	2.32	500	Replication		GoodToGo						
669	Wannisa_qPCR016_2014-12-29	3	N2	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	48	L4	20141229	169.66	4.242	1.905	2.23	2.41	500	Replication		GoodToGo						
670	Wannisa_qPCR016_2014-12-29	4	N2	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	49	L4	20141229	204.43	5.111	2.316	2.21	2.43	500	Replication		GoodToGo						
671	Wannisa_qPCR016_2014-12-29	5	N2	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	50	L4	20141229	247.96	6.199	2.818	2.2	2.4	500	Replication		GoodToGo						
672	Wannisa_qPCR016_2014-12-29	6	N2	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	52	L4	20141229	198.81	4.97	2.216	2.24	2.33	500	Replication		GoodToGo						
673	Wannisa_qPCR016_2014-12-29	7	CB4856	WT	20141223	5	Mock	20141003	0	1	NA	26	L2	54	L4	20141229	321.72	8.043	3.678	2.19	2.34	500	Replication		GoodToGo						
674	Wannisa_qPCR016_2014-12-29	8	CB4856	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	54	L4	20141229	157.42	3.936	1.762	2.23	2.34	500	Replication		GoodToGo						
675	Wannisa_qPCR016_2014-12-29	9	CB4856	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	48	L4	20141229	39	0.975	0.447	2.18	2.16	250	Replication		GoodToGo						
676	Wannisa_qPCR016_2014-12-29	10	CB4856	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	49	L4	20141229	89.13	2.228	1.034	2.16	2.24	100	Replication		GoodToGo						
677	Wannisa_qPCR016_2014-12-29	11	CB4856	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	50	L4	20141229	168.3	4.208	1.906	2.21	2.34	500	Replication		GoodToGo						
678	Wannisa_qPCR016_2014-12-29	12	CB4856	WT	20141223	5	Virus	20141003	100	1	NA	26	L2	52	L4	20141229	124.63	3.116	1.431	2.18	2.32	500	Replication		GoodToGo						
679	Wannisa_qPCR017_2015-01-07	1	N2	WT	20150106	7	Mock	20141003	0	1	NA	26	L2	45	L4	20150107	207.22	5.181	2.387	2.17	2.31	500	Replication		GoodToGo						
680	Wannisa_qPCR017_2015-01-07	2	N2	WT	20150106	7	Virus	20141003	100	1	NA	26	L2	40	L3	20150107	97.72	2.443	1.12	2.18	2.24	250	Replication		GoodToGo						
681	Wannisa_qPCR017_2015-01-07	3	N2	WT	20150106	7	Virus	20141003	100	1	NA	26	L2	42	L3	20150107	116.55	2.914	1.334	2.18	2.23	250	Replication		GoodToGo						
682	Wannisa_qPCR017_2015-01-07	4	N2	WT	20150106	7	Virus	20141003	100	1	NA	26	L2	43	L3	20150107	246.8	6.17	2.807	2.2	2.29	500	Replication		GoodToGo						
683	Wannisa_qPCR017_2015-01-07	5	N2	WT	20150106	7	Virus	20141003	100	1	NA	26	L2	45	L4	20150107	290.88	7.272	3.311	2.2	2.38	500	Replication		GoodToGo						
684	Wannisa_qPCR017_2015-01-07	6	CB4856	WT	20150106	7	Mock	20141003	0	1	NA	26	L2	45	L4	20150107	201.15	5.029	2.285	2.2	2.37	500	Replication		GoodToGo						
685	Wannisa_qPCR017_2015-01-07	7	CB4856	WT	20150106	7	Virus	20141003	100	1	NA	26	L2	40	L3	20150107	117.29	2.932	1.329	2.21	2.2	250	Replication		GoodToGo						
686	Wannisa_qPCR017_2015-01-07	8	CB4856	WT	20150106	7	Virus	20141003	100	1	NA	26	L2	42	L3	20150107	141.3	3.533	1.616	2.19	1.89	500	Replication		GoodToGo						
687	Wannisa_qPCR017_2015-01-07	9	CB4856	WT	20150106	7	Virus	20141003	100	1	NA	26	L2	43	L3	20150107	143.85	3.596	1.642	2.19	2.19	500	Replication		GoodToGo						
688	Wannisa_qPCR017_2015-01-07	10	CB4856	WT	20150106	7	Virus	20141003	100	1	NA	26	L2	45	L4	20150107	177.5	4.437	2.034	2.18	2.24	500	Replication		GoodToGo						
689	Wannisa_qPCR018_2015-01-08	1	N2	WT	20150106	6	Mock	20141003	0	1	NA	26	L2	57	Young adult	20150108	787.31	19.683	8.735	2.25	2.54	500	Replication	D1 (mock by RNA1.2) in qPCR plate was contaminated;different Ct and meliting temp --> change Ct vale to N/A	GoodToGo						
690	Wannisa_qPCR018_2015-01-08	2	N2	WT	20150106	6	Virus	20141003	100	1	NA	26	L2	33	L2	20150108	170.64	4.266	1.919	2.22	2.33	500	Replication		GoodToGo						
691	Wannisa_qPCR018_2015-01-08	3	N2	WT	20150106	6	Virus	20141003	100	1	NA	26	L2	35	L3	20150108	251.54	6.288	2.846	2.21	2.43	500	Replication		GoodToGo						
692	Wannisa_qPCR018_2015-01-08	4	N2	WT	20150106	6	Virus	20141003	100	1	NA	26	L2	47	L4	20150108	251.91	6.298	2.882	2.19	2.3	500	Replication		GoodToGo						
693	Wannisa_qPCR018_2015-01-08	5	N2	WT	20150106	6	Virus	20141003	100	1	NA	26	L2	57	Young adult	20150108	745.64	18.641	8.236	2.26	2.5	500	Replication		GoodToGo						
694	Wannisa_qPCR018_2015-01-08	6	CB4856	WT	20150106	6	Mock	20141003	0	1	NA	26	L2	57	Young adult	20150108	518.32	12.958	5.785	2.24	2.47	500	Replication		GoodToGo						
695	Wannisa_qPCR018_2015-01-08	7	CB4856	WT	20150106	6	Virus	20141003	100	1	NA	26	L2	33	L2	20150108	110.4	2.76	1.28	2.16	2.15	250	Replication		GoodToGo						
696	Wannisa_qPCR018_2015-01-08	8	CB4856	WT	20150106	6	Virus	20141003	100	1	NA	26	L2	35	L3	20150108	160.82	4.021	1.825	2.2	2.32	500	Replication		GoodToGo						
697	Wannisa_qPCR018_2015-01-08	9	CB4856	WT	20150106	6	Virus	20141003	100	1	NA	26	L2	47	L4	20150108	137.29	3.432	1.57	2.19	2.28	500	Replication		GoodToGo						
698	Wannisa_qPCR018_2015-01-08	10	CB4856	WT	20150106	6	Virus	20141003	100	1	NA	26	L2	57	Young adult	20150108	568.49	14.212	6.276	2.26	2.49	500	Replication		GoodToGo						
699	Wannisa_qPCR019_2015-01-15	1	N2	WT	20150113	8	Mock	20141003	0	1	NA	26	L2	52	L4	20150115	234.76	5.869	2.665	2.2	2.42	500	Replication		GoodToGo						
700	Wannisa_qPCR019_2015-01-15	2	N2	WT	20150113	8	Virus	20141003	100	1	NA	26	L2	28	L2	20150115	63.92	1.598	0.735	2.17	2.86	250	Replication		GoodToGo						
701	Wannisa_qPCR019_2015-01-15	3	N2	WT	20150113	8	Virus	20141003	100	1	NA	26	L2	42	L3	20150115	11:02	2.786	1.28	2.18	2.55	250	Replication		GoodToGo						
702	Wannisa_qPCR019_2015-01-15	4	N2	WT	20150113	8	Virus	20141003	100	1	NA	26	L2	45	L4	20150115	7:12	3.608	1.625	2.22	2.59	500	Replication		GoodToGo						
703	Wannisa_qPCR019_2015-01-15	5	N2	WT	20150113	8	Virus	20141003	100	1	NA	26	L2	52	L4	20150115	21:50	3.423	1.55	2.21	1.47	500	Replication		GoodToGo						
704	Wannisa_qPCR019_2015-01-15	6	CB4856	WT	20150113	8	Mock	20141003	0	1	NA	26	L2	52	L4	20150115	0:14	4.925	2.237	2.2	2.32	500	Replication		GoodToGo						
705	Wannisa_qPCR019_2015-01-15	7	CB4856	WT	20150113	8	Virus	20141003	100	1	NA	26	L2	28	L2	20150115	3:21	2.129	0.981	2.17	2.21	250	Replication		GoodToGo						
706	Wannisa_qPCR019_2015-01-15	8	CB4856	WT	20150113	8	Virus	20141003	100	1	NA	26	L2	42	L3	20150115	22:48	5.224	2.38	2.19	2.24	500	Replication		GoodToGo						
707	Wannisa_qPCR019_2015-01-15	9	CB4856	WT	20150113	8	Virus	20141003	100	1	NA	26	L2	45	L4	20150115	20:38	3.772	1.697	2.22	2.54	500	Replication		GoodToGo						
708	Wannisa_qPCR019_2015-01-15	10	CB4856	WT	20150113	8	Virus	20141003	100	1	NA	26	L2	52	L4	20150115	18:28	5.694	2.57	2.22	2.38	500	Replication		GoodToGo						
709	Wannisa_qPCR020_2015-01-26	1	N2	WT	20150122	9	Mock	20141003	0	1	NA	26	L2	53	L4	20150126	277.61	6.94	3.116	2.23	2.54	500	Replication		GoodToGo						
710	Wannisa_qPCR020_2015-01-26	2	N2	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	28	L2	20150126	140.57	3.514	1.569	2.24	2.53	500	Replication		GoodToGo						
711	Wannisa_qPCR020_2015-01-26	3	N2	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	30	L2	20150126	124.46	3.112	1.45	2.15	2.18	500	Replication		GoodToGo						
712	Wannisa_qPCR020_2015-01-26	4	N2	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	43	L3	20150126	142.47	3.562	1.603	2.22	2.48	500	Replication		GoodToGo						
713	Wannisa_qPCR020_2015-01-26	5	N2	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	50	L4	20150126	204.22	5.106	2.295	2.22	2.58	500	Replication		GoodToGo						
714	Wannisa_qPCR020_2015-01-26	6	N2	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	53	L4	20150126	298.67	7.467	3.351	2.23	2.55	500	Replication		GoodToGo						
715	Wannisa_qPCR020_2015-01-26	7	CB4856	WT	20150122	9	Mock	20141003	0	1	NA	26	L2	53	L4	20150126	256.94	6.424	2.889	2.22	2.52	500	Replication		GoodToGo						
716	Wannisa_qPCR020_2015-01-26	8	CB4856	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	28	L2	20150126	206.19	5.155	2.342	2.2	2.46	500	Replication		GoodToGo						
717	Wannisa_qPCR020_2015-01-26	9	CB4856	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	30	L2	20150126	204.13	5.103	2.32	2.2	2.46	500	Replication		GoodToGo						
718	Wannisa_qPCR020_2015-01-26	10	CB4856	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	43	L3	20150126	160.77	4.019	1.791	2.24	2.55	500	Replication		GoodToGo						
719	Wannisa_qPCR020_2015-01-26	11	CB4856	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	50	L4	20150126	124.27	3.107	1.429	2.17	2.32	500	Replication		GoodToGo						
720	Wannisa_qPCR020_2015-01-26	12	CB4856	WT	20150122	9	Virus	20141003	100	1	NA	26	L2	53	L4	20150126	208.99	5.225	2.342	2.23	2.47	500	Replication		GoodToGo						