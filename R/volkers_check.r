################################################################################
###Wetsus toxicology experiment
################################################################################


###Set your work directory
setwd("E:/Nemawork/Projects/Orsay_transcriptomics/R/")
workwd <- getwd()
filename <- "wetsus_experiment2"

###Load pre-made functions
    #uses eQTL pipeline functions https://git.wur.nl/mark_sterken/eQTL_pipeline
    #     transcriptomics functions https://git.wur.nl/mark_sterken/Transcriptomics.workbench
    #     genetic map functions https://git.wur.nl/mark_sterken/genetic.map
git_dir <- "E:/Nemawork/Projects_R_zone/Git"
source(paste(git_dir,"/NEMA_functions/Loader.R",sep=""))

###Set data locations
support_git_dir <- paste(git_dir,"/Kammenga_lab_experiments/Wetsus/supporting_files/",sep="")





################################################################################
###Dependencies
################################################################################

install <- FALSE
#.libPaths(.libPaths("C:/Program Files/R/R-3.3.3/library"))
if(install){
    install.packages("tidyverse")
    install.packages("colorspace")
    install.packages("RColorBrewer")
    install.packages("BiocInstaller",repos="http://bioconductor.org/packages/3.3/bioc")
    source("http://www.bioconductor.org/biocLite.R") ; biocLite("limma") ; biocLite("statmod")
    install.packages("gridExtra")
    install.packages("VennDiagram")
    install.packages("openxlsx")
    install.packages("rmarkdown")
}

###load
    library("colorspace")
    library("RColorBrewer")
    library(limma)
    library(gridExtra)
    library("VennDiagram")
    library(openxlsx)
    library("rmarkdown")
    library(tidyverse)


################################################################################
###Plotting theme, colours
################################################################################


###Set plotting theme
    presentation <- theme(axis.text.x = element_text(size=16, face="bold", color="black"),
                          axis.text.y = element_text(size=16, face="bold", color="black"),
                          axis.title.x = element_text(size=20, face="bold", color="black"),
                          axis.title.y = element_text(size=20, face="bold", color="black"),
                          strip.text.x = element_text(size=20, face="bold", color="black"),
                          strip.text.y = element_text(size=20, face="bold", color="black"),
                          plot.title = element_text(size=24, face="bold"),
                          panel.grid.minor = element_blank(),
                          panel.grid.major = element_blank(),
                          legend.position = "right")

    blank_theme <- theme(plot.background = element_blank(),
                         panel.grid.major = element_blank(),
                         panel.grid.minor = element_blank(),
                         panel.border = element_blank(),
                         panel.background = element_blank(),
                         axis.title.x = element_blank(),
                         axis.title.y = element_blank(),
                         axis.text.x = element_blank(),
                         axis.text.y = element_blank(),
                         axis.ticks = element_blank())


###Here you can set colours for plotting in theme using ggplot2
    #display.brewer.all()
    myColors <- c("black",brewer.pal(9,"Set1")[c(3,9)])
    
    
    names(myColors) <- c("Cis","Trans","none")
    
    colScale <- scale_colour_manual(name = "Condition",values = myColors)
    fillScale <- scale_fill_manual(name = "Condition",values = myColors)

    
################################################################################################################################################################
###load all the data                                                            ################################################################################
    
    ###load targets and read-in data
        #targets <- read.delim("W:/PROJECTS/NEMmasstorage/C.elegans/Data/MA/MA_elegans/Target_Wetsus_tox_2.txt"); head(targets)
        targets <- read.delim("D:/MA/MA_elegans/Target_NEMADAPT_RNA.txt"); head(targets)
        

            
    ###Normalize data
        ###
        rg.norm <- transcriptomics.agilent.norm(targets = targets,array_dir = "D:/MA/MA_elegans/", save_dir = paste(workwd,"/Normalized/",sep=""),)
        trans.int <- transcriptomics.transform.norm(rg.norm,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))
    
        ###Checks
        correlsums <- transcriptomics.check.cor(trans.int,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))
        transcriptomics.check.genes(trans.int,spot.id=agi.id$gene_public_name,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))
         
        ###Make a list
        colnames.sep <- ":"
        colnames.names <- c("number","Strain","batch","isolation_site","isolation_substrate","original_name","date_isolation_yyyymmdd","labcode")
    
        list.data <- transcriptomics.list.to.df(trans.int = trans.int, spot.id=Agilent.Ce.V2$SpotID,colnames.sep = colnames.sep, colnames.names = colnames.names)

        save(list.data, file=paste(workwd,"/Normalized/list.data.Rdata",sep=""))

################################################################################################################################################################
###load all the data                                                            ################################################################################

        
        data.plot <- merge(list.data,dplyr::select(Agilent.Ce.V2,SpotID,gene_public_name),by.x=2,by.y=1) %>%
                     dplyr::filter(gene_public_name%in%c("pals-25","pals-22"),Strain %in% c("N2","CB4856","WN2002"))
                
        data.plotRNA <- dplyr::select(data.plot,Strain,SpotID,gene_public_name,log2_ratio_mean) %>%
                        dplyr::rename(RNA=log2_ratio_mean,strain=Strain)
        
        pdf("Volkers2013_RNA.pdf",width=6,height=3)
        ggplot(data.plot,aes(x=Strain,y=log2_ratio_mean)) +
        geom_boxplot() + facet_grid(~gene_public_name)
        dev.off()
        
        write.xlsx(data.plot,"Volkers2013_RNA.xlsx")
        
        
        load("../../shiny_transcriptomics/Database/obj_list.data.Volkers2013.DNA.Rdata")
        
        
        
        
        data.plot <- merge(list.data.Volkers2013.DNA,dplyr::select(Agilent.Ce.V2,SpotID,gene_public_name),by.x=2,by.y=1) %>%
                     dplyr::filter(gene_public_name%in%c("pals-25","pals-22"),strain %in% c("N2","CB4856","WN2002"))
                
        
        data.plotDNA <- dplyr::select(data.plot,strain,SpotID,gene_public_name,log2_ratio_mean) %>%
                        dplyr::rename(DNA=log2_ratio_mean)    
        
        
        data.test <- merge(data.plotRNA,data.plotDNA)
        
        
        summary(lm(RNA~DNA+strain,data=dplyr::filter(data.test,gene_public_name=="pals-22")))
        
        
        ggplot(data.plot,aes(x=strain,y=log2_ratio_mean)) +
        geom_boxplot() + facet_grid(~gene_public_name)
        
        
        pdf("Volkers2013_DNA.pdf",width=6,height=3)
        ggplot(data.plot,aes(x=strain,y=log2_ratio_mean)) +
        geom_boxplot() + facet_grid(~gene_public_name)
        dev.off()
        
        write.xlsx(data.plot,"Volkers2013_DNA.xlsx")
        
        
        