################################################################################
###Wetsus toxicology experiment
################################################################################


###Set your work directory
setwd("E:/Nemawork/Projects/shiny_transcriptomics/Volkers_etal_2013")
workwd <- getwd()
filename <- "Volkers_2013"

###Load pre-made functions
    #uses eQTL pipeline functions https://git.wur.nl/mark_sterken/eQTL_pipeline
    #     transcriptomics functions https://git.wur.nl/mark_sterken/Transcriptomics.workbench
    #     genetic map functions https://git.wur.nl/mark_sterken/genetic.map
git_dir <- "E:/Nemawork/Projects_R_zone/Git"
source(paste(git_dir,"/Loader_git.R",sep=""))

###Set data locations
#support_git_dir <- paste(git_dir,"/Kammenga_lab_experiments/Wetsus/supporting_files/",sep="")





################################################################################
###Dependencies
################################################################################

install <- FALSE
#.libPaths(.libPaths("C:/Program Files/R/R-3.3.3/library"))
if(install){
    install.packages("tidyverse")
    install.packages("colorspace")
    install.packages("RColorBrewer")
    install.packages("BiocInstaller",repos="http://bioconductor.org/packages/3.3/bioc")
    source("http://www.bioconductor.org/biocLite.R") ; biocLite("limma") ; biocLite("statmod")
    install.packages("gridExtra")
    install.packages("VennDiagram")
    install.packages("openxlsx")
    install.packages("rmarkdown")
}

###load
    library("colorspace")
    library("RColorBrewer")
    library(limma)
    library(gridExtra)
    library("VennDiagram")
    library(openxlsx)
    library("rmarkdown")
    library(tidyverse)


################################################################################
###Plotting theme, colours
################################################################################


###Set plotting theme
    presentation <- theme(axis.text.x = element_text(size=16, face="bold", color="black"),
                          axis.text.y = element_text(size=16, face="bold", color="black"),
                          axis.title.x = element_text(size=20, face="bold", color="black"),
                          axis.title.y = element_text(size=20, face="bold", color="black"),
                          strip.text.x = element_text(size=20, face="bold", color="black"),
                          strip.text.y = element_text(size=20, face="bold", color="black"),
                          plot.title = element_text(size=24, face="bold"),
                          panel.grid.minor = element_blank(),
                          panel.grid.major = element_blank(),
                          legend.position = "right")

    blank_theme <- theme(plot.background = element_blank(),
                         panel.grid.major = element_blank(),
                         panel.grid.minor = element_blank(),
                         panel.border = element_blank(),
                         panel.background = element_blank(),
                         axis.title.x = element_blank(),
                         axis.title.y = element_blank(),
                         axis.text.x = element_blank(),
                         axis.text.y = element_blank(),
                         axis.ticks = element_blank())


###Here you can set colours for plotting in theme using ggplot2
    #display.brewer.all()
    myColors <- c("black",brewer.pal(9,"Set1")[c(3,9)])
    
    
    names(myColors) <- c("Cis","Trans","none")
    
    colScale <- scale_colour_manual(name = "Condition",values = myColors)
    fillScale <- scale_fill_manual(name = "Condition",values = myColors)

    
################################################################################
###normalization of DNA data
################################################################################
    targets <- read.delim(file="W:/PROJECTS/NEMmasstorage/C.elegans/Data/MA/MA_elegans/Target_NEMADAPT_DNA.txt")

    rg.norm <- agilent.limma.norm(targets = targets,
                                  array_dir = "W:/PROJECTS/NEMmasstorage/C.elegans/Data/MA/MA_elegans/", 
                                  save_dir = paste(workwd,"/Normalized/",sep=""),
                                  bg.method = "subtract",
                                  nBA.method = "scale",
                                  filename = filename)
    
    trans.int <- transform.norm(rg.norm,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))

    ###Checks
    #correlsums <- correlation.check(trans.int,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))
    #genes.check(trans.int,spot.id=agi.id$gene_public_name,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))

    ###Make a list
    colnames.sep <- ":"
    colnames.names <- c("number","strain","isolation_site","isolation_substrate","original_name","date_isolation_yyyymmdd","labcode")

    
    list.data.Volkers2013.DNA <- list.to.dataframe(trans.int = trans.int, spot.id=agi.id$SpotID,colnames.sep = colnames.sep, colnames.names = colnames.names)
    
    
    save(list.data.Volkers2013.DNA ,file=paste(workwd,"/Normalized/obj_list.data.Volkers2013.DNA.Rdata",sep=""))
    
    spots <- read.delim("Probes_checked_for_linear_model.txt") %>%
             filter(!duplicated(paste(Sequence,term))) %>%
             select(-c(CHR:known_function))
 
    data.plot <- filter(list.data.Volkers2013.DNA,strain %in% c("N2","CB4856")) %>%
                 merge(Agilent.Ce.V2[,c(1,18,19)],by.x=2,by.y=1) %>%
                 merge(spots,by.x=1,by.y=1) %>%
                 select(SpotID,term,chromosome,gene_bp_start,gene_bp_end,gene_sequence_name,gene_WBID,gene_public_name,Sequence,log2_intensities,strain) %>%
                 spread(key=strain,value=log2_intensities) %>%
                 mutate(differential_hybridization_CB4856_N2=CB4856-N2,auf=as.numeric(gsub("AGIWUR","",SpotID))) %>%
                 arrange(auf) %>%
                 select(-CB4856,-N2,-auf)
    
    write.xlsx(data.plot,file="DNA_hyb.xlsx") 
      
    data.plot <- filter(list.data.Volkers2013.DNA,SpotID %in% filter(Agilent.Ce.V2,grepl("pals",gene_public_name))$SpotID,strain %in% c("N2","CB4856")) %>%
                 merge(Agilent.Ce.V2,by.x=2,by.y=1) %>%
                 select(SpotID,chromosome,gene_bp_start,gene_bp_end,gene_sequence_name,gene_WBID,gene_public_name,Sequence,log2_intensities,strain) %>%
                 spread(key=strain,value=log2_intensities) %>%
                 mutate(differential_hybridization_CB4856_N2=CB4856-N2,auf=as.numeric(gsub("AGIWUR","",SpotID))) %>%
                 arrange(auf) %>%
                 select(-CB4856,-N2,-auf)
    
    write.xlsx(data.plot,file="DNA_hyb_pals.xlsx") 
    
    data.volkers <- data.plot
    
    save(data.volkers,file="obj_data.volkers.Rdata")
    pdf(file="pals_genes.pdf",width=7,height=7)
        ggplot(data.plot,aes(y=strain,x=gene_public_name,fill=ratio)) +
        geom_raster() + facet_grid(~chromosome,scales = "free_x",space="free_x") + scale_fill_gradientn(colours=brewer.pal(11,"RdYlBu")) +
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
    dev.off()
    
    data.plot <- filter(list.data,SpotID %in% filter(Agilent.Ce.V2,grepl("pals",gene_public_name))$SpotID) %>%
                 merge(Agilent.Ce.V2,by.x=2,by.y=1) %>%
                 group_by(gene_public_name,strain,chromosome,gene_bp_start) %>%
                 summarise(ratio=mean(log2_ratio_mean)) %>%
                 filter(strain %in% c("N2","CB4856"))

    pdf(file="pals_genes_CB4856_N2.pdf",width=7,height=7)
        ggplot(data.plot,aes(y=strain,x=gene_public_name,fill=ratio)) +
        geom_raster() + facet_grid(~chromosome,scales = "free_x",space="free_x") + scale_fill_gradientn(colours=brewer.pal(11,"RdYlBu")) +
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
    dev.off()    
        
    data.plot <- filter(list.data,SpotID %in% filter(Agilent.Ce.V2,grepl("pals",gene_public_name))$SpotID) %>%
                 merge(Agilent.Ce.V2,by.x=2,by.y=1) %>%
                 group_by(gene_public_name,strain,chromosome,gene_bp_start) %>%
                 summarise(ratio=mean(log2_ratio_mean)) %>%
                 filter(gene_public_name %in% c("pals-22","pals-25")) %>%
                 arrange(ratio) %>%
                 data.frame() %>%
                 mutate(strain=factor(strain,levels = unique(strain)))

    pdf(file="pals_genes_pals22-25.pdf",width=12,height=3)
        ggplot(data.plot,aes(x=strain,y=ratio,colour=strain%in%c("N2","CB4856"))) +
        geom_point() + facet_grid(~gene_public_name) +
        theme(axis.text.x = element_text(angle = 45, hjust = 1))
    dev.off()
    
    
    
################################################################################
###Summarize data for array express
################################################################################
        
    ###Array express
        tmp <- separate(data.frame(tmp=c(as.character(unlist(targets$Cy3)),as.character(unlist(targets$Cy5)))),tmp,into=c("strain","isolation_site","isolation_substrate","original_name","date_isolation_yyyymmdd","labcode"),sep=":")
        tmp <- cbind(FileName=rep(targets$FileName,times=2),label=rep(c("Cy3","Cy5"),each=nrow(targets)),tmp)
  
        Samples <- data.frame(tmp) %>%
                   mutate(Organism="Caenorhabditis elegans",
                          Organism_part="whole organism",
                          Sex="hermaphrodite") %>%
                   separate(FileName,into=c("folder","file"),sep="Raw")
    
        write.xlsx(Samples,file="ArrayExpress.xlsx")
            
        for(i in c(1:24)){file.copy(from=paste("W:/PROJECTS/NEMmasstorage/C.elegans/Data/MA/MA_elegans/",targets[i,3],sep=""), to=workwd)} 

        datanorm <-cbind('Reporter Identifier'=1:45220,rg.norm)
            colnames(datanorm)[-1] <- paste("Sample ",1:(ncol(datanorm)-1),sep="")
        
        write.table(datanorm,file="normalized_data.txt",sep = "\t",quote=F,row.names = F)
        
        
        
################################################################################
###normalization of expression data
################################################################################
    filename <- "Volkers_2013_RNA"
    targets <- read.delim(file="W:/PROJECTS/NEMmasstorage/C.elegans/Data/MA/MA_elegans/Target_NEMADAPT_RNA.txt")


    rg.norm <- agilent.limma.norm(targets = targets,
                                  array_dir = "W:/PROJECTS/NEMmasstorage/C.elegans/Data/MA/MA_elegans/", 
                                  save_dir = paste(workwd,"/Normalized/",sep=""),
                                  filename = filename)
    
    trans.int <- transform.norm(rg.norm,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))

    ###Checks
    #correlsums <- correlation.check(trans.int,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))
    #genes.check(trans.int,spot.id=agi.id$gene_public_name,filename=filename,save_dir=paste(workwd,"/Normalized/",sep=""))

    ###Make a list
    colnames.sep <- ":"
    colnames.names <- c("number","strain","batch","isolation_site","isolation_substrate","original_name","date_isolation_yyyymmdd","labcode")

    
    list.data.Volkers2013.RNA <- list.to.dataframe(trans.int = trans.int, spot.id=agi.id$SpotID,colnames.sep = colnames.sep, colnames.names = colnames.names)

    save(list.data.Volkers2013.RNA ,file=paste(workwd,"/Normalized/obj_list.data.Volkers2013.RNA.Rdata",sep=""))
 
    
    
################################################################################
###Summarize data for array express
################################################################################

    ###Array express
        tmp <- separate(data.frame(tmp=c(as.character(unlist(targets$Cy3)),as.character(unlist(targets$Cy5)))),tmp,into=c("strain","batch","isolation_site","isolation_substrate","original_name","date_isolation_yyyymmdd","labcode"),sep=":")
        tmp <- cbind(FileName=rep(targets$FileName,times=2),label=rep(c("Cy3","Cy5"),each=nrow(targets)),tmp)
  
        Samples <- data.frame(tmp) %>%
                   mutate(Organism="Caenorhabditis elegans",
                          Age=47,
                          Organism_part="whole organism",
                          Environmental_history="After bleaching, eggs were hatched and grown at 20 degrees Celsius on nematode growth medium containing OP50",
                          Developmental_stage="L4 larvae",
                          Sex="hermaphrodite",
                          Diet="Escherichia coli OP50") %>%
                   separate(FileName,into=c("folder","file"),sep="Raw")
    
        write.xlsx(Samples,file="ArrayExpress.xlsx")
            
                    
        for(i in c(1:48)){file.copy(from=paste("W:/PROJECTS/NEMmasstorage/C.elegans/Data/MA/MA_elegans/",targets[i,3],sep=""), to=workwd)} 

        datanorm <-cbind('Reporter Identifier'=1:45220,rg.norm)
            colnames(datanorm)[-1] <- paste("Sample ",1:(ncol(datanorm)-1),sep="")
        
        write.table(datanorm,file="normalized_data.txt",sep = "\t",quote=F,row.names = F)
               
        